import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CustomComponent } from './components/custom/custom.component';
import { HomeComponent } from './components/home/home.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { StreetComponent } from './components/street/street.component';
import { TouringComponent } from './components/touring/touring.component';
import { TrailComponent } from './components/trail/trail.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'cadastro-clientes', component: CadastroClientesComponent},
  {path: 'cadastro-motos', component: CadastroMotosComponent},
  {path: 'cadastro-usuarios', component: CadastroUsuarioComponent},
  {path: 'custom', component: CustomComponent},
  {path: 'scooter', component: ScooterComponent},
  {path: 'sport', component: SportComponent},
  {path: 'street', component: StreetComponent},
  {path: 'touring', component: TouringComponent},
  {path: 'trail', component: TrailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
