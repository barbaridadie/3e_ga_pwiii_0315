import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestaquesComponent } from './destaques/destaques.component';



@NgModule({
  declarations: [
    DestaquesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
