import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompanhamentosPedidosComponent } from './acompanhamentos-pedidos.component';

describe('AcompanhamentosPedidosComponent', () => {
  let component: AcompanhamentosPedidosComponent;
  let fixture: ComponentFixture<AcompanhamentosPedidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcompanhamentosPedidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompanhamentosPedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
