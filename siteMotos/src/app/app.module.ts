import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TrailComponent } from './components/trail/trail.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { AcompanhamentosPedidosComponent } from './components/acompanhamentos-pedidos/acompanhamentos-pedidos.component';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TrailComponent,
    StreetComponent,
    CustomComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    CadastroClientesComponent,
    CadastroUsuarioComponent,
    CadastroMotosComponent,
    AcompanhamentosPedidosComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
